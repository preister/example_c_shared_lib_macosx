#!/bin/bash

gcc -c -Wall -Werror -fpic shared.c

gcc -shared -o libshared.so shared.o

gcc -Wall -o hello hello.c -lshared -L`pwd` -Wl,-rpath,.